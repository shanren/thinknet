﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 管理员
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminUser : EntityBase
    {
        /// <summary>
        /// 登陆名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 登陆密码
        /// </summary>
        public string LoginPwd { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegTime { get; set; }
        /// <summary>
        /// 最后登陆时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }
        /// <summary>
        /// 最后登陆IP
        /// </summary>
        public string LastLoginIP { get; set; }
        /// <summary>
        /// 登陆次数
        /// </summary>
        public string LoginTimes { get; set; }
        /// <summary>
        /// 管理员状态
        /// </summary>
        public bool State { get; set; }
    }
}
