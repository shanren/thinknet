﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Data;
using Think.Permission.Model;
using Think.Web.App_Code;

namespace Think.Web.Admin.Permission
{
    public partial class AdminRole_Edit : AdminPage
    {
        AdminRole adminRole;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((adminRole = GetModelByID<AdminRole>()) != null)
                {
                    LoadData(this.Controls, adminRole);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if ((adminRole = GetModelByID<AdminRole>()) == null)
            {
                adminRole = new AdminRole();
            }
            LoadEntity(this.Controls, adminRole);
            TRuntime.GetDB().Save(adminRole);
            ShowAlert("操作完成", true);
        }
    }
}