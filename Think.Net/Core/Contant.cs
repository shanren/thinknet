﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Think.Core
{
   /// <summary>
   /// 系统常量
   /// </summary>
   public class Constants
    {
       /// <summary>
       /// 标记方法返回 Void
       /// </summary>
       public class VoidResult { };
       /// <summary>
       /// 标记方法返回 Void
       /// </summary>
       public static readonly VoidResult Void = new VoidResult();
       /// <summary>
       /// 默认时间
       /// </summary>
       public static DateTime DefaultDateTime = new DateTime(1900, 1, 1);
       /// <summary>
       /// 默认数据库驱动：System.Data.SqlClient
       /// </summary>
       public static string ProviderMsSql = "System.Data.SqlClient";
       /// <summary>
       /// 默认Redis驱动：XRedis
       /// </summary>
       public static string ProviderRedis = "XRedis";
    }
}
