﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using System.Web.UI;
using System.ComponentModel;
using Think.Model;

namespace Think.Controls
{
    /// <summary>
    /// 后台控件查询接口
    /// </summary>
    public interface ISearcher
    {
        /// <summary>
        /// 获取或设置搜索分组
        /// </summary>
        string SearcherGroupName { get; set; }

        /// <summary>
        /// 获取或设置sql条件字段
        /// </summary>
        string SqlField { get; set; }

        /// <summary>
        /// 获取或设置sql条件关系符
        /// </summary>
        string SqlCharacter { get; set; }

        /// <summary>
        /// 获取控件值
        /// </summary>
        /// <returns></returns>
        string GetValue();
    }

    /// <summary>
    /// 查询条件集合
    /// </summary>
    public class SearcherColletion 
    {
        /// <summary>
        /// 获取查询条件，注意使用此方法控件的SqlCharacter直接写逻辑
        /// </summary>
        /// <param name="contain"></param>
        /// <param name="eo"></param>
        /// <param name="searcherGroupName"></param>
        /// <param name="IsAccurate"></param>
        /// <returns></returns>
        public static string GetWhere(Control contain, string searcherGroupName = "", bool IsAccurate = false)
        {
            FilterList fl = new FilterList();
            List<ISearcher> lstSearchers = new List<ISearcher>();
            if (contain.HasControls())
            {
                GetControls(contain, lstSearchers, searcherGroupName);
            }
            lstSearchers.ForEach(c =>
            {
                if (IsAccurate)
                {
                    if (string.Compare(c.SqlCharacter, "like", true) == 0)
                    {
                        c.SqlCharacter = "=";
                    }
                }
                var co = new FilterObject()
                   {
                       Field = c.SqlField,
                       Comparer = c.SqlCharacter,
                       Value = c.GetValue()
                   };
                fl.Add(co);

            });
            return fl.ToString();
        }

        private static void GetControls(Control parent, List<ISearcher> lstSearchers, string groupName = "")
        {
            ControlCollection controls = parent.Controls;
            foreach (Control ctl in controls)
            {
                if (ctl is ISearcher)
                {
                    var searcher = ctl as ISearcher;
                    if (string.IsNullOrWhiteSpace(groupName))
                    {
                        lstSearchers.Add(searcher);
                    }
                    else if (searcher.SearcherGroupName == groupName)
                    {
                        lstSearchers.Add(searcher);
                    }
                }
                else if (ctl.HasControls())
                {
                    GetControls(ctl,lstSearchers, groupName);
                }
            }
        }


        #region 查询条件集合

        /// <summary>
        /// 查询条件集合
        /// </summary>
        public class FilterList
        {
            /// <summary>
            /// 查询条件集合
            /// </summary>
            public List<FilterObject> InnerList = new List<FilterObject>(5);
            FilterObject LastFilter = null;
            /// <summary>
            /// 新增条件
            /// </summary>
            /// <param name="obj"></param>
            public void Add(FilterObject obj)
            {
                obj.IsLast = true;
                this.InnerList.Add(obj);
                if (LastFilter != null)
                {
                    LastFilter.IsLast = false;
                }
                this.LastFilter = obj;
            }
            public override string ToString()
            {
                if (this.InnerList.Count == 0)
                {
                    return "1=1";
                }
                this.LastFilter = this.InnerList[InnerList.Count - 1];
                this.LastFilter.IsLast = true;
                List<string> r = this.InnerList.ConvertAll<string>((fo) =>
                {
                    return fo.ToString();
                });
                return string.Join(" ", r.ToArray());
            }
        }

        #endregion

        #region 单个查询条件

        /// <summary>
        /// 单个查询条件
        /// </summary>
        public class FilterObject
        {
            string _Field = "";
            public string Field
            {
                get { return _Field; }
                set { _Field = value; }
            }

            string _Value = "";
            public string Value
            {
                get { return _Value; }
                set { _Value = value; }
            }
            string _Comparer = "";
            public string Comparer
            {
                get
                {
                    if (string.IsNullOrEmpty(_Comparer))
                    {
                        _Comparer = " = ";
                    }
                    return _Comparer;
                }
                set { _Comparer = value; }
            }

            string _Logic = "";
            public string Logic
            {
                get
                {
                    if (this.IsLast)
                    {
                        return "";
                    }
                    if (string.IsNullOrEmpty(_Logic))
                    {
                        _Logic = " and ";
                    }
                    return _Logic;
                }
                set { _Logic = value; }
            }

            public bool IsLast = false;
            public override string ToString()
            {
                string tmpl = "{0} {1} '{2}' {3}";
                //处理like，startwith,endwith etc
                if (string.Compare(Comparer.Trim(), "like", true) == 0)
                {
                    tmpl = "{0} {1} '%{2}%' {3}";
                }
                string strreturn = string.Format(tmpl, Field, Comparer, Value, Logic);
                if (string.IsNullOrWhiteSpace(Value) && IsLast)
                {
                    return "1=1";
                }
                else if (string.IsNullOrWhiteSpace(Value))
                {
                    return "";
                }
                else if (Value == "on")
                {
                    Value = "1";
                    strreturn = string.Format(tmpl, Field, Comparer, Value, Logic);
                    return strreturn;
                }
                else
                {
                    return strreturn;
                }
            }
        }

        #endregion
    }
}
