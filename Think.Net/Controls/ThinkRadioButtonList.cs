﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace Think.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class ThinkRadioButtonList : FineUI.RadioButtonList, ISearcher
    {
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置搜索分组")]
        public string SearcherGroupName { get; set; }
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件字段")]
        public string SqlField { get; set; }
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件关系符")]
        public string SqlCharacter { get; set; }
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]绑定的数据源表名")]
        public string TableName { get; set; }
        
        /// <summary>
        /// 获取控件值
        /// </summary>
        /// <returns></returns>
        public string GetValue()
        {
            return this.SelectedValue;
        }
    }
}
