﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 管理员功能菜单树
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminFuncTree : EntityBase
    {
        /// <summary>
        /// 功能名称
        /// </summary>
        public string FuncName { get; set; }
        /// <summary>
        /// 功能地址
        /// </summary>
        public string FuncUrl { get; set; }
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int FuncParentID { get; set; }
        /// <summary>
        /// 功能节点Logo
        /// </summary>
        public string FuncLogo { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int SortIdx { get; set; }
        /// <summary>
        /// 操作
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// 本菜单在树形结构中层级（从0开始）
        /// </summary>
        public int TreeLevel { get; set; }
        
    }
}
