﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Think.Web.Admin.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ThinkNet框架</title>
    <style type="text/css">
            body.f-theme-neptune .header { background-color: #005999; }
            body.f-theme-neptune .header .x-panel-body { background-color: transparent; }
            body.f-theme-neptune .header .title a { font-weight: bold; font-size: 18px; text-decoration: none; line-height: 40px; margin-left: 10px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" AutoSizePanelID="regionPanel" runat="server" />
        <f:RegionPanel ID="regionPanel" ShowBorder="false" runat="server">
            <Regions>
                <f:Region ID="regionTop" ShowBorder="false" ShowHeader="false" CssClass="header"  Position="Top" Layout="Fit" runat="server">
                    <Toolbars>
                        <f:Toolbar ID="Toolbar1" Position="Bottom" runat="server" CssStyle="background-color: #005999;">
                            <Items>
                                <f:ContentPanel ID="ContentPanel1" ShowBorder="false" ShowHeader="false" runat="server">
                                    <div class="title"><a href="main.aspx" style="color: #fff;">ThinkNet框架</a></div>
                                </f:ContentPanel>
                                <f:ToolbarFill ID="ToolbarFill1" runat="server" />
                                <f:ToolbarText ID="txtUser" runat="server" CssStyle="color: #fff;"></f:ToolbarText>
                                <f:Button ID="btnExit" runat="server" Icon="UserRed" Text="安全退出" ConfirmText="确定退出系统？" OnClick="btnExit_Click">
                                </f:Button>
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                </f:Region>
                <f:Region ID="regionLeft" Split="true" Icon="Outline" EnableCollapse="true" Width="200px" ShowHeader="true" Title="系统菜单" Layout="Fit" Position="Left" runat="server">
                </f:Region>
                <f:Region ID="mainRegion" ShowHeader="false" Layout="Fit" Margin="0 0 0 0" Position="Center" runat="server">
                    <Items>
                        <f:TabStrip ID="mainTabStrip" EnableTabCloseMenu="true" ShowBorder="false" runat="server">
                            <Tabs>
                                <f:Tab ID="Tab1" Title="首页" Layout="Fit" Icon="House" runat="server">
                                    <Items>
                                        
                                    </Items>
                                </f:Tab>
                            </Tabs>
                        </f:TabStrip>
                    </Items>
                </f:Region>
            </Regions>
        </f:RegionPanel>
        <f:Window ID="Window1" runat="server" IsModal="false" Hidden="true" EnableIFrame="true"
            EnableResize="true" EnableMaximize="true" IFrameUrl="about:blank" Width="650px"
            Height="450px">
        </f:Window>
    </form>
    <script type="text/javascript">
        var menuClientID = '<%=treeMenu==null?null: treeMenu.ClientID %>';
        var tabStripClientID = '<%=mainTabStrip==null?null: mainTabStrip.ClientID %>';

        // 页面控件初始化完毕后，会调用用户自定义的onReady函数
        F.ready(function () {
            var mainMenu = F(IDS.mainMenu);
            var mainTabStrip = F(IDS.mainTabStrip);
            var leftRegion = F(IDS.leftRegion);

            // 初始化主框架中的树(或者Accordion+Tree)和选项卡互动，以及地址栏的更新
            // treeMenu： 主框架中的树控件实例，或者内嵌树控件的手风琴控件实例
            // mainTabStrip： 选项卡实例
            // createToolbar： 创建选项卡前的回调函数（接受tabConfig参数）
            // updateLocationHash: 切换Tab时，是否更新地址栏Hash值
            // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame
            // refreshWhenTabChange: 切换选项卡时，是否刷新内部IFrame
            F.util.initTreeTabStrip(F(menuClientID), F(tabStripClientID), null, false, true, false);

            // 添加标签页
            window.addExampleTab = function (id, url, text, icon, refreshWhenExist) {
                // 动态添加一个标签页
                // mainTabStrip： 选项卡实例
                // id： 选项卡ID
                // url: 选项卡IFrame地址 
                // text： 选项卡标题
                // icon： 选项卡图标
                // addTabCallback： 创建选项卡前的回调函数（接受tabConfig参数）
                // refreshWhenExist： 添加选项卡时，如果选项卡已经存在，是否刷新内部IFrame
                F.util.addMainTab(mainTabStrip, id, url, text, icon, null, refreshWhenExist);
            };

            // 移除选中标签页
            window.removeActiveTab = function () {
                var activeTab = mainTabStrip.getActiveTab();
                mainTabStrip.removeTab(activeTab.id);
            };

            //刷新tab
            window.reloadTab = function (id) {
                var tab = mainTabStrip.getTab(id);
                if (tab) {
                    var iframe = Ext.DomQuery.selectNode('iframe', tab.body.dom);
                    iframe.src = tab.url; // iframe.src;
                }
            }
        });
    </script>
</body>
</html>
