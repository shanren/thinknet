﻿using FineUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Permission;
using Think.Permission.Model;

namespace Think.Web.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string loginName = txtUserName.Text.Trim();
            string loginPwd = txtPassword.Text.Trim();
            AdminUser user = Manage.AdminLogin(loginName, loginPwd);
            if (user!=null)
            {
                HttpContext.Current.Session["AdminUser"] = user;
                Response.Redirect("Main.aspx", false);
            }
            else
            {
                Alert.Show("登录出错");
            }
        }
    }
}