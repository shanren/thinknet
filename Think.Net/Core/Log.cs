﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Think.Utils;

namespace Think.Core
{
    /// <summary>
    /// 日志
    /// </summary>
    public class Log
    {
        private static string serPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "log\\" + DateTime.Now.ToString("yyyy-MM-dd");   //日志文件路径,事先指定              

        /// <summary>
        /// 写日志[通过队列方式]
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="type">类型</param>
        public static void Write(string message, string type)
        {
            QueueTask.Add<string, string>(Log.WriteFile, message, type);
        }

        /// <summary>                                                
        /// 写日志
        /// </summary>                                                                                                                                       
        public static void WriteFile(string message, string type)
        {
            try
            {
                PathExists(serPath);
                string fname = serPath + "\\" + type + ".txt";   //指定日志文件的目录  
                FileInfo finfo = new FileInfo(fname);
                using (FileStream fs = finfo.OpenWrite())
                {
                    StreamWriter w = new StreamWriter(fs);
                    w.BaseStream.Seek(0, SeekOrigin.End);            //设置写数据流的起始位置为文件流的末尾  
                    w.Write("\r\n");
                    w.Write("时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    w.Write("\r\n");
                    //w.Write("等级：" + level.ToString());
                    //w.Write("\r\n");
                    w.Write("内容：" + message + "\r\n");           //写入错误信息  
                    w.Write("\r\n");
                    w.Flush();                                      //清空缓冲区内容，并把缓冲区内容写入基础流           
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 检查日志目录是否存在，如果不存在则建立文件夹
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static void PathExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }

    /// <summary>
    /// 日志等级
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// 严重错误，导致系统崩溃无法使用
        /// </summary>
        EMERG,
        /// <summary>
        /// 警戒性错误， 必须被立即修改的错误
        /// </summary>
        ALERT,
        /// <summary>
        /// 临界值错误， 超过临界值的错误
        /// </summary>
        CRIT,
        /// <summary>
        /// 一般性错误
        /// </summary>
        ERR,
        /// <summary>
        /// 警告性错误， 需要发出警告的错误
        /// </summary>
        WARN,
        /// <summary>
        /// 通知，程序可以运行但是还不够完美的错误
        /// </summary>
        NOTICE,
        /// <summary>
        /// 信息，程序输出信息
        /// </summary>
        INFO,
        /// <summary>
        /// 调试，用于调试信息
        /// </summary>
        DEBUG,
        /// <summary>
        /// SQL语句，该级别只在调试模式开启时有效
        /// </summary>
        SQL
    }

}
