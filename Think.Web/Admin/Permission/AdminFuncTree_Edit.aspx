﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminFuncTree_Edit.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminFuncTree_Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" runat="server"></f:PageManager>
        <f:Form ID="FormSubmit" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px" Title="SimpleForm">
            <Toolbars>
                <f:Toolbar ID="Toolbar1" runat="server">
                    <Items>
                        <f:Button ID="btnSave" Icon="DatabaseSave" runat="server" Text="保存"  OnClick="btnSave_Click" ValidateForms="FormSubmit"></f:Button>
                    </Items>
                </f:Toolbar>
            </Toolbars>
            <Rows>
                <f:FormRow>
                    <Items>
                        <f:ToolbarText Text="非开发人员勿动" CssStyle="text-align:center;margin-bottom:10px;font-size:16px; font-weight:bold;color:red;" ID="TitleBar" runat="server"></f:ToolbarText>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkDropDownList ID="ddlParent" SqlField="FuncParentID" Label="上级菜单" EnableSimulateTree="true" runat="server">
                        </t:ThinkDropDownList>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtFunctionName" SqlField="FuncName" MaxLength="32" runat="server" Label="功能名称" Required="true" ShowRedStar="true">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtFunctionUrl" SqlField="FuncUrl" MaxLength="256" runat="server" Label="功能地址">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkNumberBox ID="txtSort" SqlField="SortIdx" runat="server" Label="排序" Text="0">
                        </t:ThinkNumberBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkCheckBox ID="cbIsShow" SqlField="IsShow" runat="server" Label="是否显示" Text="显示"></t:ThinkCheckBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <f:CheckBoxList ID="cblFunction" runat="server" Label="通用操作">
                            <f:CheckItem Value="btnNew" Text="添加"></f:CheckItem>
                            <f:CheckItem Value="btnUpdate" Text="编辑"></f:CheckItem>
                            <f:CheckItem Value="btnDelete" Text="删除"></f:CheckItem>
                            <f:CheckItem Value="btnView" Text="查看"></f:CheckItem>
                            <f:CheckItem Value="btnFreeze" Text="冻结"></f:CheckItem>
                            <f:CheckItem Value="btnOpen" Text="开通"></f:CheckItem>
                            <f:CheckItem Value="btnExcel" Text="导出"></f:CheckItem>
                            <f:CheckItem Value="btnImport" Text="导入"></f:CheckItem>
                        </f:CheckBoxList>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <f:TextBox ID="txtFunction" runat="server" Label="自定义操作">
                        </f:TextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <f:Label runat="server" Text='格式如:"发短信:btnSend;"'></f:Label>
                    </Items>
                </f:FormRow>
            </Rows>
        </f:Form>
    </form>
</body>
</html>
