﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FineUI;
using System.ComponentModel;
namespace Think.Controls
{
    /// <summary>
    /// CheckBox控件
    /// </summary>
    public class ThinkCheckBox : FineUI.CheckBox
    {
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件字段")]
        public string SqlField { get; set; }
    }
}
