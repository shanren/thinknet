﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminUser_List.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminUser_List" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server"></f:PageManager>
        <f:Panel ID="Panel1" runat="server" BodyPadding="5px" ShowBorder="false" Layout="VBox" BoxConfigAlign="Stretch" BoxConfigPosition="Start" ShowHeader="false" Title="用户管理">
            <Items>
                <f:Form ID="SearchForm" runat="server" BodyPadding="5px" ShowHeader="false" ShowBorder="false">
                    <Rows>
                        <f:FormRow>
                            <Items>
                                <t:ThinkTextBox ID="txtLoginName" runat="server" SearcherGroupName="basic" SqlCharacter="like" Label="登录账号" SqlField="LoginName">
                                </t:ThinkTextBox>
                                <f:Button ID="btnSeach" runat="server" Text="搜索" OnClick="btnSearch_Click"></f:Button>
                            </Items>
                        </f:FormRow>
                    </Rows>
                </f:Form>
                <t:ThinkGrid ID="GridList" EnableTextSelection="true" runat="server" BoxFlex="1" ShowBorder="true" ShowHeader="false" EnableCheckBoxSelect="true" DataKeyNames="ID" AllowPaging="true" IsDatabasePaging="true" EnableMultiSelect="true">
                    <Toolbars>
                        <f:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <f:Button ID="btnNew" runat="server" Icon="Add" Text="新增" OnClick="btnOpenDialog_Click">
                                </f:Button>
                                <f:Button ID="btnUpdate" runat="server" Icon="Pencil" Text="编辑" OnClick="btnOpenDialog_Click">
                                </f:Button>
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <PageItems>
                        <f:ToolbarText ID="ToolbarText1" runat="server" Text="每页记录数："></f:ToolbarText>
                        <t:ThinkDropDownList ID="ddlGridPageSize" Width="80px" runat="server"></t:ThinkDropDownList>
                    </PageItems>
                    <Columns>
                        <f:BoundField DataField="LoginName" Width="120px" HeaderText="登录账号"></f:BoundField>
                        <f:BoundField DataField="RealName" Width="100px" HeaderText="真实姓名"></f:BoundField>
                        <f:BoundField DataField="Phone" Width="120px" HeaderText="联系电话"></f:BoundField>
                        <f:BoundField DataField="State" Width="100px" HeaderText="账号状态"></f:BoundField>
                        <f:BoundField DataField="LastLoginIP" Width="150px" HeaderText="最后登录IP"></f:BoundField>
                        <f:BoundField DataField="LastLoginTime" Width="150px" HeaderText="最后登录时间"></f:BoundField>
                        <f:BoundField DataField="RegTime" Width="150px" HeaderText="注册时间"></f:BoundField>
                    </Columns>
                </t:ThinkGrid>
            </Items>
        </f:Panel>
        <f:Window ID="WindowOpen" runat="server" IsModal="false" Hidden="true" Target="Self" OnClose="Window_Close" EnableIFrame="true" EnableResize="true" EnableMaximize="true" IFrameUrl="about:blank" Width="850px" Height="400px">
        </f:Window>
    </form>
</body>
</html>
