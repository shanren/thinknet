﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FineUI;
using System.ComponentModel;

namespace Think.Controls
{
    public class ThinkTextBox : FineUI.TextBox, ISearcher
    {
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置搜索分组")]
        public string SearcherGroupName { get; set; }
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件字段")]
        public string SqlField { get; set; }
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件关系符")]
        public string SqlCharacter { get; set; }

        /// <summary>
        /// 获取控件值
        /// </summary>
        /// <returns></returns>
        public string GetValue()
        {
            return Text.Trim();
        }
    }
}
