﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminFuncTree_List.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminFuncTree_List" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" AutoSizePanelID="Panel1" runat="server"></f:PageManager>
        <f:Panel ID="Panel1" runat="server" BodyPadding="5px" ShowBorder="false" ShowHeader="false" Layout="Fit">
            <items>
                <f:Grid ID="GridList" EnableTextSelection="true" runat="server" ShowBorder="true" ShowHeader="false" EnableCollapse="true" EnableCheckBoxSelect="false" DataKeyNames="ID">
                    <Toolbars>
                        <f:Toolbar ID="Toolbar1" runat="server">
                            <Items>
                                <f:Button ID="btnNew" runat="server" Icon="Add" Text="新增" OnClick="btnOpenDialog_Click">
                                </f:Button>
                                <f:Button ID="btnUpdate" runat="server" Icon="Pencil" Text="编辑" OnClick="btnOpenDialog_Click">
                                </f:Button>
                            </Items>
                        </f:Toolbar>
                    </Toolbars>
                    <Columns>
                         <f:BoundField DataField="FuncName" HeaderText="功能名称" DataSimulateTreeLevelField="TreeLevel" Width="200px"></f:BoundField>
                         <f:BoundField DataField="FuncUrl" HeaderText="URL" Width="500px"></f:BoundField>
                         <f:CheckBoxField DataField="IsShow" HeaderText="是否显示" Width="100px" RenderAsStaticField="true"></f:CheckBoxField>
                         <f:BoundField DataField="SortIdx" HeaderText="排序" Width="80px"></f:BoundField>
                    </Columns>
                </f:Grid>
            </items>
        </f:Panel>

        <f:Window ID="WindowOpen" runat="server" IsModal="false" Hidden="true" Target="Self" OnClose="Window_Close" EnableIFrame="true" EnableResize="true" EnableMaximize="true" IFrameUrl="about:blank" Width="850px" Height="520px"></f:Window>
    </form>
</body>
</html>
