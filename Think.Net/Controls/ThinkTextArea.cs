﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Think.Controls
{
    public class ThinkTextArea : FineUI.TextArea
    {
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件字段")]
        public string SqlField { get; set; }
    }
}
