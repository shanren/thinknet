﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminRole_List.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminRole_List" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" runat="server" AutoSizePanelID="RegionPanel1" />
        <f:RegionPanel ID="RegionPanel1" ShowBorder="false" runat="server">
            <regions>
                <f:Region ID="Region1" ShowBorder="false" ShowHeader="false" Width="200px" RegionPosition="Left" Layout="Fit" runat="server">
                    <Items>
                        <f:Grid ID="GridRoles" ShowBorder="true" ShowHeader="true" Title="管理员角色" runat="server" OnRowSelect="GridRoles_RowSelect"
                            DataKeyNames="ID" EnableMultiSelect="false" EnableRowSelectEvent="true" ShowGridHeader="false">
                            <Columns>
                                <f:BoundField ExpandUnusedSpace="true" DataField="Name" DataFormatString="{0}" HeaderText="角色名称" />
                            </Columns>
                        </f:Grid>
                    </Items>
                </f:Region>
                <f:Region ID="Region2" ShowBorder="false" ShowHeader="false" Position="Center" Layout="VBox" BoxConfigAlign="Stretch" BoxConfigPosition="Left" runat="server">
                    <Toolbars>
                <f:Toolbar ID="Toolbar1" runat="server">
                    <Items>
                        <f:Button ID="btnSave" Icon="GroupEdit" runat="server" Text="更新当前角色的权限" OnClick="btnSave_Click"></f:Button>
                                <f:Button ID="btnNew" runat="server" Icon="Add" Text="新增角色" OnClick="btnOpenDialog_Click" ></f:Button>
                                <f:Button ID="btnUpdate" runat="server" Icon="Pencil" Text="编辑角色" OnClick="btnOpenDialog_Click" ></f:Button>
                    </Items>
                </f:Toolbar>
            </Toolbars>
                     <Items>
                        <f:Tree ID="FuncTree" ShowHeader="true" Width="600px"  EnableCollapse="true" Title="功能树" runat="server">
                            <Nodes>
                            </Nodes>
                        </f:Tree>
                    </Items>
                </f:Region>
            </regions>
        </f:RegionPanel>
        <f:Window ID="WindowOpen" runat="server" IsModal="false" Hidden="true" Target="Self" OnClose="Window_Close" EnableIFrame="true" EnableResize="true" EnableMaximize="true" IFrameUrl="about:blank" Width="850px" Height="300px"></f:Window>
    </form>
</body>
</html>
