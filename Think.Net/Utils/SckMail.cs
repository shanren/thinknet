using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

namespace Think.Utils
{
    /// <summary>
    /// TcpClient派生类，用来进行SMTP服务器的连接工作
    /// </summary>
    public class SMTPClient : TcpClient
    {
        /// <summary>
        /// 是否链接
        /// </summary>
        /// <returns></returns>
        public bool isConnected()
        {
            return Active;
        }

        /// <summary>
        /// 向服务器发送指令
        /// </summary>
        /// <param name="Command">指令内容</param>
        public void SendCommandToServer(string Command)
        {
            NetworkStream ns = this.GetStream();
            byte[] WriteBuffer = null;
            WriteBuffer = new byte[Command.Length];
            WriteBuffer = Encoding.Default.GetBytes(Command);
            ns.Write(WriteBuffer, 0, WriteBuffer.Length);
            return;
        }

        /// <summary>
        /// 返回服务器处理结果
        /// </summary>
        /// <returns></returns>
        public string GetServerResponse()
        {
            int StreamSize;
            string ReturnValue = "";
            byte[] ReadBuffer;
            NetworkStream ns = this.GetStream();

            ReadBuffer = new byte[1024];
            StreamSize = ns.Read(ReadBuffer, 0, ReadBuffer.Length);
            if(StreamSize == 0)
            {
                return ReturnValue;
            }
            else
            {
                ReturnValue = Encoding.Default.GetString(ReadBuffer);
                return ReturnValue;
            }
        }

        /// <summary>
        /// 生成SMTPCode
        /// </summary>
        /// <param name="s">信息内容</param>
        /// <param name="SMTPCode">SMTPCode</param>
        /// <returns></returns>
        public bool DoesStringContainSMTPCode(string s, string SMTPCode)
        {
            return (s.IndexOf(SMTPCode, 0, 10) == -1) ? false : true;
        }

    } //结束类

    /// <summary>
    /// 发送邮件类
    /// </summary>
    public class SckMail
    {
        /// <summary>
        /// 邮件发送优先级权举表
        /// </summary>
        public enum Prioritys
        {
            /// <summary>
            /// 最高级别
            /// </summary>
            HIGH = 1,
            /// <summary>
            /// 默认级别
            /// </summary>
            NORMAL = 3,
            /// <summary>
            /// 最低级别
            /// </summary>
            LOW = 5
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public SckMail()
        {
            strErrMessage = "";
            strResponse = "";
        }
        
        #region "返回信息"

        /// <summary>
        /// SMTP服务器反馈的信息
        /// </summary>
        private string strResponse = "";

        /// <summary>
        /// 取得SMTP服务器反馈的信息
        /// </summary>
        public string ServerResponse
        {
            get
            {
                return strResponse;
            }
        }

        /// <summary>
        /// 错误反馈信息
        /// </summary>
        private string strErrMessage = "";

        /// <summary>
        /// 取得错误反馈信息
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return strErrMessage;
            }
        }

        /// <summary>
        /// 认证用户名
        /// </summary>
        private string strUsername = "";

        /// <summary>
        /// 认证用户名
        /// </summary>
        public string Username
        {
            get { return strUsername; }
            set { strUsername = value; }
        }

        /// <summary>
        /// 认证密码
        /// </summary>
        private string strPassword = "";

        /// <summary>
        /// 认证密码
        /// </summary>
        public string Password
        {
            get { return strPassword; }
            set { strPassword = value; }
        }

        #endregion

        #region "邮件属性"
        
        /// <summary>
        /// SMTP 邮件服务器
        /// </summary>
        private string _SmtpHost = "";

        /// <summary>
        /// SMTP 邮件服务器
        /// </summary>
        public string SmtpHost
        {
            get
            {
                return _SmtpHost;
            }
            set
            {
                _SmtpHost = value;
            }
        }
        
        /// <summary>
        /// SMTP邮件服务器端口
        /// </summary>
        private int _Port = 25;

        /// <summary>
        /// SMTP邮件服务器端口
        /// </summary>
        public int Port
        {
            get
            {
                return _Port;
            }
            set
            {
                _Port = value;
            }
        }
        
        #endregion

        /// <summary>
        /// Socket发送邮件
        /// </summary>
        /// <param name="strFrom">发送人 邮件地址</param>
        /// <param name="strDisplayFromName">发送人 显示名</param>
        /// <param name="strTo">收件人 邮件地址</param>
        /// <param name="strDisplayToName">收件人 显示名</param>
        /// <param name="clsPriority">邮件优先级</param>
        /// <param name="blnHtml">是否发送HTML邮件</param>
        /// <param name="strBase">发送来源(可以为空)</param>
        /// <param name="strMessage">发送内容</param>
        /// <param name="strSubject">邮件主题</param>
        public void SendMail(string strFrom, string strDisplayFromName, string strTo, string strDisplayToName, 
            Prioritys clsPriority, bool blnHtml, string strBase, string strSubject, string strMessage)
        {
            string strResponseNumber = "";
            bool bolConnect = false;
            List<string> strSendBuffer = null;
            string[] strResponseCode = { "220", "235", "250", "251", "354", "334", "221" };   // success codes from SMTP server

            try
            {
                strSendBuffer = new List<string>();

                SMTPClient smtpcMail = new SMTPClient();
                
                smtpcMail.Connect(_SmtpHost, _Port);
                bolConnect = smtpcMail.isConnected();

                //判断是否进行了连接
                if(!bolConnect)
                {
                    strErrMessage = "Smtp服务器连接失败...";
                    return;
                }

                //读取反馈信息
                strResponseNumber = smtpcMail.GetServerResponse();
                if(smtpcMail.DoesStringContainSMTPCode(strResponseNumber, "220"))
                {
                    this.strResponse += strResponseNumber;
                }
                else
                {
                    this.strErrMessage = "连接失败:" + strResponseNumber;
                    return;
                }

                string strData = "";
                strData = string.Concat("HELO ", _SmtpHost);
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat(strData, "AUTH LOGIN");
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat(strData, Encrypt.EncodeBase64(this.strUsername));
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat(strData, Encrypt.EncodeBase64(this.strPassword));
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat("MAIL FROM: ", "<" + strFrom + ">");
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat("RCPT TO: ", "<" + strTo + ">");
                strData = string.Concat(strData, "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat("DATA", "\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat("From: ", strDisplayFromName + "<" + strFrom + ">");
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "To: ");
                strData = string.Concat(strData, strDisplayToName + "<" + strTo + ">");
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "Subject: ");
                strData = string.Concat(strData, strSubject);
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "MIME-Version: 1.0");
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "X-Priority: " + clsPriority);
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "X-MSMail-Priority: " + clsPriority);
                strData = string.Concat(strData, "\r\n");
                if(blnHtml == true)
                {
                    strData = string.Concat(strData, "Content-Type: text/html;");
                }
                else
                {
                    strData = string.Concat(strData, "Content-Type: text/plain;");
                }
                strData = string.Concat(strData, "charset=\"gb2312\"");
                strData = string.Concat(strData, "\r\n");
                strData = string.Concat(strData, "Content-Transfer-Encoding: text/html");
                strData = string.Concat(strData, "\r\n" + "\r\n");

                strData = string.Concat(strData, strMessage);
                strData = string.Concat(strData, "\r\n.\r\n");
                strSendBuffer.Add(strData);

                strData = "";
                strData = string.Concat(strData, "QUIT\r\n");
                strSendBuffer.Add(strData);
                int i = 0;

                while(i < strSendBuffer.Count)
                {
                    smtpcMail.SendCommandToServer(strSendBuffer[i]);
                    strResponseNumber = smtpcMail.GetServerResponse();

                    for(int j = 0; j < strResponseCode.Length; j++)
                    {
                        if(smtpcMail.DoesStringContainSMTPCode(strResponseNumber, strResponseCode[j]))
                        {
                            this.strResponse += strResponseNumber;
                            this.strResponse += "<br>";
                            break;
                        }
                        else
                        {
                            if(j == strResponseCode.Length - 1)
                            {
                                this.strErrMessage += strResponseNumber;
                                this.strErrMessage += strSendBuffer[i];
                                return;
                            }
                        }
                    }

                    i++;
                } // 结束循环
            }
            catch(SocketException err)
            {
                this.strErrMessage += err.Message + " " + err.StackTrace;
                throw (err);
            }
            catch(Exception e)
            {
                this.strErrMessage += e.Message + " " + e.StackTrace;
                throw (e);
            }
        }

    }
}
