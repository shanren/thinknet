﻿//关闭tab
function closeActiveTab() {
    parent.removeActiveTab();
}
//tab形式打开页面
function AddNewTab(id, url, text, icon, refreshWhenTabChange) {
    parent.addExampleTab.apply(null, [id, url, text, icon, refreshWhenTabChange]);
}

//刷新tab
function ReloadTab(id) {
    parent.reloadTab(id);
}

//下载
function DownLoad(link) {
    window.open("/admin/DownLoad.ashx/DownFile?link=" + link, "_self");
}