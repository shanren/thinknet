﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 管理员角色映射
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminRoleMap : EntityBase
    {
        /// <summary>
        /// 管理员ID
        /// </summary>
        public int AdminID { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool State { get; set; }
    }
}
