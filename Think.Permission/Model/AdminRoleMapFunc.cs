﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 管理员角色映射功能树
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminRoleMapFunc : EntityBase
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }
        /// <summary>
        /// 功能节点树ID
        /// </summary>
        public int FuncID { get; set; }
        
    }
}
