﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Permission;
using Think.Web.App_Code;

namespace Think.Web.Admin.Permission
{
    public partial class AdminFuncTree_List : AdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected override void BindGrid()
        {
            var list= Manage.GetAllFuncTree();
            if (list != null && list.Count > 0)
            {
                GridList.RecordCount = list.Count;
                GridList.DataSource = list;
            }
            else
            {
                GridList.RecordCount = 0;
                GridList.DataSource = null;
            }
            GridList.DataBind();
        }

        protected void btnOpenDialog_Click(object sender, EventArgs e)
        {
            OpenDialogClick(sender, "AdminFuncTree_Edit.aspx", "菜单编辑", GridList, WindowOpen);
        }
    }
}