﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Think.Common
{
    /// <summary>
    /// 结构类型
    /// </summary>
    public enum ResultType
    {
        Success,
        Error
    }

    /// <summary>
    /// 发布给客户端的结果数据
    /// </summary>
    public class CommonResult
    {
        /// <summary>
        /// 结果类型枚举
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 代码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 数据（需可序列化）
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 创建结果对象
        /// </summary>
        /// <param name="type"></param>
        /// <param name="msg"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static CommonResult Create(string type, string msg = "", string code = "", object data = null)
        {
            return new CommonResult()
            {
                Type = type,
                Msg = msg,
                Code = code,
                Data = data
            };
        }

        /// <summary>
        /// 返回成功
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string CreateSuccess(string msg, object data = null, string code = null)
        {
            CommonResult cResult = Create(ResultType.Success.ToString(), msg, code, data);
            return Newtonsoft.Json.JsonConvert.SerializeObject(cResult);
        }

        /// <summary>
        /// 返回失败
        /// </summary>
        /// <returns></returns>
        public static string CreateError(string msg = "", object data = null, string code = "")
        {
            CommonResult cResult = Create(ResultType.Error.ToString(), msg, code, data);
            return Newtonsoft.Json.JsonConvert.SerializeObject(cResult);
        }

    }
}
