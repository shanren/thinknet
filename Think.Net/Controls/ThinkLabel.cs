﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FineUI;
using System.ComponentModel;
namespace Think.Controls
{
    /// <summary>
    /// 标签
    /// </summary>
    public class ThinkLabel : FineUI.Label
    {
        [Category("属性")]
        [DefaultValue("")]
        [Description("[AJAX属性]获取或设置sql条件字段")]
        public string SqlField { get; set; }
    }
}
