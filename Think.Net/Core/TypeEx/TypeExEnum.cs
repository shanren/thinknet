﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQT.Core.TypeEx
{
    /// <summary>
    /// TypeCode 扩展
    ///string 之前为Sytem.TypeCode
    /// </summary>
    public enum TypeCodeEx
    {
        /// <summary>
        /// 空引用
        /// </summary>
        Empty = 0,
        /// <summary>
        /// 常规类型，表示不会由另一个 TypeCode 显式表示的任何引用或值类型。
        /// </summary>
        Object = 1,
        /// <summary>
        /// 数据库空（列）值。
        /// </summary>
        DBNull = 2,
        /// <summary>
        /// 简单类型，表示 true 或 false 的布尔值。
        /// </summary>
        Boolean = 3,
        /// <summary>
        /// 整型，表示值介于 0 到 65535 之间的无符号 16 位整数。 System.TypeCode.Char 类型的可能值集与 Unicode 字符集相对应。
        /// </summary>
        Char = 4,
        /// <summary>
        /// 整型，表示值介于 -128 到 127 之间的有符号 8 位整数。
        /// </summary>
        SByte = 5,
        /// <summary>
        ///  整型，表示值介于 0 到 255 之间的无符号 8 位整数。
        /// </summary>
        Byte = 6,
        /// <summary>
        ///  整型，表示值介于 -32768 到 32767 之间的有符号 16 位整数。
        /// </summary>

        Int16 = 7,
        /// <summary>
        ///  整型，表示值介于 0 到 65535 之间的无符号 16 位整数。
        /// </summary>

        UInt16 = 8,
        /// <summary>
        ///   整型，表示值介于 -2147483648 到 2147483647 之间的有符号 32 位整数。
        /// </summary>
        Int32 = 9,
        /// <summary>
        ///  整型，表示值介于 0 到 4294967295 之间的无符号 32 位整数。
        /// </summary>
        UInt32 = 10,
        /// <summary>
        /// 整型，表示值介于 -9223372036854775808 到 9223372036854775807 之间的有符号 64 位整数。
        /// </summary>
        Int64 = 11,
        /// <summary>
        ///   整型，表示值介于 0 到 18446744073709551615 之间的无符号 64 位整数。
        /// </summary>
        UInt64 = 12,
        /// <summary>
        /// 浮点型，表示从大约 1.5 x 10 -45 到 3.4 x 10 38 且精度为 7 位的值。
        /// </summary>
        Single = 13,
        /// <summary>
        /// 浮点型，表示从大约 5.0 x 10 -324 到 1.7 x 10 308 且精度为 15 到 16 位的值。
        /// </summary>

        Double = 14,
        /// <summary>
        ///  简单类型，表示从 1.0 x 10 -28 到大约 7.9 x 10 28 且有效位数为 28 到 29 位的值。
        /// </summary>
        Decimal = 15,
        /// <summary>
        /// 表示一个日期和时间值的类型。
        /// </summary>
        DateTime = 16,
        /// <summary>
        /// 密封类类型，表示 Unicode 字符串。
        /// </summary>
        String = 18,
        /// <summary>
        /// GUID
        /// </summary>
        Guid = 51,
        /// <summary>
        /// 
        /// </summary>
        TimeSpan = 52,

        /// <summary>
        /// 指针
        /// </summary>
        IntPtr = 101,
        /// <summary>
        /// 指针
        /// </summary>
        UIntPtr = 102,
        /// <summary>
        /// 221-227 集合
        /// </summary>
        IList = 221,
        /// <summary>
        /// 字典
        /// </summary>
        IDictionary = 222,
        /// <summary>
        /// 集合
        /// </summary>
        ICollection = 223,
        /// <summary>
        /// 泛型集合
        /// </summary>
        ICollectionT = 224,
        /// <summary>
        /// 泛型列表
        /// </summary>
        IListT = 225,
        /// <summary>
        /// 泛型字典
        /// </summary>
        IDictionaryT = 226,
        /// <summary>
        /// 枚举集合
        /// </summary>
        IEnumerable = 227,
        /// <summary>
        /// 
        /// </summary>
        DbDataReader = 228,
        /// <summary>
        /// 
        /// </summary>
        DataSet = 229,
        /// <summary>
        /// 
        /// </summary>
        DataTable = 230,
        /// <summary>
        /// 
        /// </summary>
        DataView = 231,
        /// <summary>
        /// 
        /// </summary>
        AnonymousType = 232,
        /// <summary>
        /// 
        /// </summary>
        Enum = 233,
        /// <summary>
        /// 
        /// </summary>
        Type = 234,
        /// <summary>
        /// 
        /// </summary>
        DbParameter = 245,
        /// <summary>
        /// 
        /// </summary>
        StringBuilder = 250,
        /// <summary>
        /// 
        /// </summary>
        Xml = 246,
    }


}
