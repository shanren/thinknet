﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Text;
using Think.Web.App_Code;
using Think.Permission;
using Think.Permission.Model;
using FineUI;
using System.Linq;
using Think.Utils;

namespace Think.Web.Admin.Permission
{
    public partial class AdminRole_List : AdminPage
    {
        public static string RoleID = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected override void BindGrid()
        {
            var list = Manage.GetAllRoles();
            GridRoles.DataSource = list;
            GridRoles.DataBind();

            BindTree(list[0].ID.ToString());
        }

        protected void GridRoles_RowSelect(object sender, FineUI.GridRowSelectEventArgs e)
        {
            string roleID = GridRoles.DataKeys[GridRoles.SelectedRowIndex][0].ToString();
            BindTree(roleID);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string[] nodeIds = FuncTree.GetCheckedNodeIDs();
            if (nodeIds == null || nodeIds.Length == 0)
            {
                Alert.Show("请选择功能后再保存");
                return;
            }

            List<AdminRoleMapFunc> list = new List<AdminRoleMapFunc>();
            for (int i = 0; i < nodeIds.Length; i++)
            {
                if (nodeIds[i] != "0")
                {
                    AdminRoleMapFunc rolefunc = new AdminRoleMapFunc();
                    rolefunc.RoleID =TypeTo.ToInt(RoleID);
                    rolefunc.FuncID =TypeTo.ToInt(nodeIds[i]);
                    list.Add(rolefunc);
                }
            }
            Manage.SaveRoleMapFunc(list, RoleID);

            Alert.Show("操作完成");
        }

        protected void btnOpenDialog_Click(object sender, EventArgs e)
        {
            OpenDialogClick(sender, "AdminRole_Edit.aspx", "菜单编辑", GridRoles, WindowOpen);
        }
        
        public void BindTree(string roleID)
        {
            RoleID = roleID;
            List<AdminFuncTree> allfunc = Manage.GetAllFuncTree2();//TODO
            List<AdminFuncTree> listcheck = Manage.GetFuncTreeByRoleID(roleID);
            InitTreeMenu(allfunc, listcheck);
        }

        #region 生成树

        /// <summary>
        /// 创建树菜单
        /// </summary>
        /// <param name="menus"></param>
        /// <returns></returns>
        private void InitTreeMenu(List<AdminFuncTree> menus, List<AdminFuncTree> menusCheck)
        {
            ResolveMenuTree(menus, menusCheck, 0, FuncTree.Nodes);
        }

        /// <summary>
        /// 生成菜单树
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="parentMenuId"></param>
        /// <param name="nodes"></param>
        private void ResolveMenuTree(List<AdminFuncTree> menus, List<AdminFuncTree> menusCheck, int parentMenuId, FineUI.TreeNodeCollection nodes)
        {
            foreach (AdminFuncTree menu in menus)
            {
                if (menu.FuncParentID == parentMenuId)
                {
                    FineUI.TreeNode node = new FineUI.TreeNode();
                    node.Expanded = false;
                    node.EnableCheckEvent = true;
                    node.EnableCheckBox = true;
                    node.Text = menu.FuncName;
                    node.NodeID = menu.ID.ToString();
                    if (menusCheck.Where(c => c.ID.ToString() == node.NodeID).Count() > 0)
                    {
                        node.Checked = true;
                    }
                    nodes.Add(node);
                    ResolveMenuTree(menus, menusCheck, menu.ID, node.Nodes);
                }
            }
        }

        #endregion
    }
}