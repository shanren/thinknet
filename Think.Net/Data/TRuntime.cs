﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Think.Data
{
    /// <summary>
    /// 数据库操作工具
    /// </summary>
    public class TRuntime 
    {
        static Dictionary<int, Database> dicDB = new Dictionary<int, Database>();

        /// <summary>
        /// 获取数据库
        /// </summary>
        /// <param name="dbNo">数据库编号</param>
        /// <returns></returns>
        public static Database GetDB(int dbNo = 1)
        {
            Database db;
            if (dicDB.ContainsKey(dbNo) && dicDB[dbNo] != null)
            {
                db = dicDB[dbNo];
            }
            else
            {
                string dbConfig = dbNo.ToString() + "_DbConfig";
                db = new Database(dbConfig);
                db.OpenSharedConnection();
                dicDB[dbNo] = db;
            }
            return db;
        }

    }
}
