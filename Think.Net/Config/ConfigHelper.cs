﻿using System;
using System.Configuration;
using XQT.Cache;
using System.Linq;

namespace Think.Config
{
    /// <summary>
    /// web.config操作类
    /// </summary>
    public sealed class ConfigHelper
    {
        /// <summary>
        /// 得到AppSettings中的配置字符串信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigString(string key)
        {
            string CacheKey = "AppSettings-" + key;
            object objModel = "";
            try
            {
                objModel = ConfigurationManager.AppSettings[key];
                if (objModel != null)
                {
                    HttpCache.Set(CacheKey, objModel, DateTime.Now.AddMinutes(180));
                }

            }
            catch
            {
                HttpCache.Set(CacheKey, "", DateTime.Now.AddMinutes(180));
            }
            return objModel != null ? objModel.ToString() : "";
        }

        /// <summary>
        /// 得到AppSettings中的配置Bool信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetConfigBool(string key)
        {
            bool result = false;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = bool.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }
            return result;
        }
        /// <summary>
        /// 得到AppSettings中的配置Decimal信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static decimal GetConfigDecimal(string key)
        {
            decimal result = 0;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = decimal.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }

            return result;
        }
        /// <summary>
        /// 得到AppSettings中的配置int信息
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetConfigInt(string key)
        {
            int result = 0;
            string cfgVal = GetConfigString(key);
            if (null != cfgVal && string.Empty != cfgVal)
            {
                try
                {
                    result = int.Parse(cfgVal);
                }
                catch (FormatException)
                {
                    // Ignore format exceptions.
                }
            }

            return result;
        }


        /// <summary>
        /// 获取连接字符串
        /// </summary>
        /// <param name="providerName">默认：System.Data.SqlClient</param>
        /// <param name="config">string|int(负数从最后一项开始)</param>
        /// <returns></returns>
        public static string GetConnection(string providerName = "System.Data.SqlClient", object config = null)
        {
            ConnectionStringSettings setting = null;
            if (config == null)
            {
                setting = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().Where(c => c.ProviderName == providerName).LastOrDefault();
            }
            else
            {
                if (config is string)
                {
                    var configName = config + "";
                    setting = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().Where(c => c.Name == configName && c.ProviderName == providerName).LastOrDefault();
                }
                else if (config is int)
                {
                    var idx = (int)config;
                    if (idx < 0)
                    {
                        idx = ConfigurationManager.ConnectionStrings.Count + idx;
                    }
                    setting = ConfigurationManager.ConnectionStrings[idx];
                }
            }
            if (setting != null)
            {
                return setting.ConnectionString;
            }
            return null;
        }

        /// <summary>
        /// 获取设置
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static ConnectionStringSettings GetConnectionSetting(string providerName = "System.Data.SqlClient", object config = null)
        {
            ConnectionStringSettings setting = null;
            if (config == null)
            {
                setting = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().Where(c => c.ProviderName == providerName).LastOrDefault();
            }
            else
            {
                if (config is string)
                {
                    var configName = config + "";
                    setting = ConfigurationManager.ConnectionStrings.OfType<ConnectionStringSettings>().Where(c => c.Name == configName && c.ProviderName == providerName).LastOrDefault();
                }
                else if (config is int)
                {
                    var idx = (int)config;
                    if (idx < 0)
                    {
                        idx = ConfigurationManager.ConnectionStrings.Count + idx;
                    }
                    setting = ConfigurationManager.ConnectionStrings[idx];
                }
            }
            return setting;
        }

    }
}
