﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Permission.Model;
using Think.Utils;

namespace Think.Permission
{
    /// <summary>
    /// 管理类
    /// </summary>
    public class Manage
    {
        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="loginPwd"></param>
        /// <returns></returns>
        public static AdminUser AdminLogin(string loginName, string loginPwd)
        {
            string _loginPwd = Encrypt.MD5(loginPwd,true);
            string sql = "select * from AdminUser where LoginName=@0 and LoginPwd=@1 and State=1";
            AdminUser adminUser = TRuntime.GetDB().FirstOrDefault<AdminUser>(sql, loginName, _loginPwd);
            if (adminUser != null)
            {
                return adminUser;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 用户名是否存在
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static bool IsExistName(string loginName)
        {
            if (TRuntime.GetDB().FirstOrDefault<AdminUser>("where LoginName=@0", loginName) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static bool ExitLogin(string loginName)
        {
            return true;
        }
        

        /// <summary>
        /// 检查页面权限
        /// </summary>
        /// <param name="adminName"></param>
        /// <param name="pageUrl"></param>
        /// <returns></returns>
        public static bool CheckPagePermission(string adminID,string pageUrl)
        {
            return true;
        }

        /// <summary>
        /// 检查按钮操作权限
        /// </summary>
        /// <param name="adminName"></param>
        /// <param name="pageUrl"></param>
        /// <param name="actionID"></param>
        /// <returns></returns>
        public static bool CheckActionPermission(int adminID, string pageUrl,string actionID)
        {
            return true;
        }


        #region 角色

        public static List<AdminRole> GetAllRoles()
        {
            var ls = TRuntime.GetDB().Fetch<AdminRole>("select * from AdminRole");
            return ls;
        }

        public static List<AdminRoleMap> GetUserRoles(string adminID)
        {
            var ls = TRuntime.GetDB().Fetch<AdminRoleMap>("select * from AdminRoleMap where AdminID=@0 and State=1", adminID);
            return ls;
        }

        public static void SetUserRoles(int adminID,string[] rolesID)
        {
            var ls = TRuntime.GetDB().Fetch<AdminRoleMap>("select * from AdminRoleMap where AdminID=@0", adminID);
            foreach (AdminRoleMap role in ls)
            {
                role.State = false;
                TRuntime.GetDB().Update(role);
            }
            for (int i = 0; i < rolesID.Length; i++)
            {
                AdminRoleMap role = new AdminRoleMap();
                role.RoleID=TypeTo.ToInt(rolesID[i]);
                role.AdminID = adminID;
                role.State = true;
                TRuntime.GetDB().Insert(role);
            }
        }

        #endregion

        #region 菜单

        /// <summary>
        /// 获取用户所有功能树节点
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public static List<AdminFuncTree> GetAdminTree(int adminID)
        {
            string sql = " select * from AdminFuncTree where ID in(select distinct FuncID from AdminRoleMapFunc where RoleID in(select RoleID from AdminRoleMap where AdminID=" + adminID + " and State=1)) and TreeLevel<2 and IsShow=1 order by SortIdx desc";
            var list = TRuntime.GetDB().Fetch<AdminFuncTree>(sql);
            return list;
        }

        /// <summary>
        /// 获取所有功能树节点;排除操作节点
        /// </summary>
        /// <returns></returns>
        public static List<AdminFuncTree> GetAllFuncTree()
        {
            string sql = " select * from AdminFuncTree where TreeLevel<2 ";
            var list = TRuntime.GetDB().Fetch<AdminFuncTree>(sql);

            List<AdminFuncTree> listTree = new List<AdminFuncTree>();
            List<AdminFuncTree> _list = list.FindAll(c => c.FuncParentID == 0);
            foreach (AdminFuncTree t in _list)
            {
                var _t = list.FindAll(c => c.FuncParentID == t.ID);
                listTree.Add(t);
                _t.ForEach(c => listTree.Add(c));
            }
            return listTree;
        }

        /// <summary>
        /// 获取所有功能树节点
        /// </summary>
        /// <returns></returns>
        public static List<AdminFuncTree> GetAllFuncTree2()
        {
            string sql = " select * from AdminFuncTree ";
            var list = TRuntime.GetDB().Fetch<AdminFuncTree>(sql);
            return list;
        }

        /// <summary>
        /// 获取功能树中的操作节点
        /// </summary>
        /// <returns></returns>
        public static List<AdminFuncTree> GetActionTree(string treeID)
        {
            string sql = "select * from AdminFuncTree where FuncParentID='" + treeID + "' and TreeLevel=2";
            var list = TRuntime.GetDB().Fetch<AdminFuncTree>(sql);
            return list;
        }

        /// <summary>
        /// 保存功能树操作节点
        /// </summary>
        /// <param name="listTree"></param>
        /// <param name="treeID"></param>
        public static void SaveFuncTree(List<AdminFuncTree> listTree, string treeID)
        {
            List<AdminFuncTree> listHave = GetActionTree(treeID);
            foreach (AdminFuncTree tree in listTree)
            {
                var t = listHave.Find(c => c.Action == tree.Action && c.FuncParentID == tree.FuncParentID);
                if (t == null)
                    TRuntime.GetDB().Insert(tree);
            }
            foreach (AdminFuncTree tree in listHave)
            {
                if (listTree.Exists(c => c.FuncParentID == tree.FuncParentID && c.Action == tree.Action) == false)
                {
                    TRuntime.GetDB().Delete(tree);
                    TRuntime.GetDB().Execute(" delete from AdminRoleMapFunc where FuncID='" + tree.ID + "' ");
                }
            }
        }

        /// <summary>
        /// 通过角色获取功能树
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public static List<AdminFuncTree> GetFuncTreeByRoleID(string roleID)
        {
            string sql = "select * from AdminFuncTree where ID in(select FuncID from AdminRoleMapFunc where RoleID=" + roleID + ")";
            return TRuntime.GetDB().Fetch<AdminFuncTree>(sql);
        }

        /// <summary>
        /// 保存角色对应的功能树
        /// </summary>
        /// <param name="list"></param>
        /// <param name="roleID"></param>
        public static void SaveRoleMapFunc(List<AdminRoleMapFunc> list,string roleID)
        {
            var listHave = GetRoleMapFuncByRoleID(roleID);
            foreach (AdminRoleMapFunc mapfunc in list)
            {
                if (!listHave.Exists(c => c.FuncID == mapfunc.FuncID))
                {
                    TRuntime.GetDB().Insert(mapfunc);
                }
            }
            foreach (AdminRoleMapFunc mapfunc in listHave)
            {
                if (!list.Exists(c => c.FuncID == mapfunc.FuncID))
                {
                    TRuntime.GetDB().Delete(mapfunc);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public static List<AdminRoleMapFunc> GetRoleMapFuncByRoleID(string roleID)
        {
            string sql = "select * from AdminRoleMapFunc where RoleID=" + roleID + "";
            return TRuntime.GetDB().Fetch<AdminRoleMapFunc>(sql);
        }

        #endregion

    }
}
