﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminUser_Edit.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminUser_Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" runat="server"></f:PageManager>
        <f:Form ID="FormSubmit" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px" Title="SimpleForm">
            <Toolbars>
                <f:Toolbar ID="Toolbar1" runat="server">
                    <Items>
                        <f:Button ID="btnSave" Icon="DatabaseSave" runat="server" Text="保存" OnClick="btnSave_Click" ValidateForms="FormSubmit">
                        </f:Button>
                    </Items>
                </f:Toolbar>
            </Toolbars>
            <Rows>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtLoginname" SqlField="LoginName" runat="server" Label="登录账号" MaxLength="50" Required="true" ShowRedStar="true">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtPwd" runat="server" Label="登录密码" TextMode="Password" MinLength="6" MaxLength="20">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <f:TextBox ID="txtPwd2" runat="server" Label="确认密码" TextMode="Password" CompareControl="txtPwd">
                        </f:TextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtRealName" SqlField="RealName" runat="server" Label="真实姓名" MaxLength="20" Required="true" ShowRedStar="true">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtTel" SqlField="Phone" MaxLength="32" runat="server" Label="联系电话" Regex="^((\d{3,4}-\d{7,8})|(\d{11}))$" RegexMessage="电话格式不正确">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkDropDownList ID="ddlRoles" runat="server" Label="所属角色" EnableMultiSelect="true" Required="true" ShowRedStar="true" AutoSelectFirstItem="false"></t:ThinkDropDownList>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkCheckBox ID="ckUserStatus" runat="server" Label="是否启用" SqlField="State"></t:ThinkCheckBox>
                    </Items>
                </f:FormRow>
            </Rows>
        </f:Form>
    </form>
</body>
</html>
