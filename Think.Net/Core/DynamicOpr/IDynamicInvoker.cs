﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Think.Core.DynamicOpr
{
    /// <summary>
    ///  
    /// </summary>
    /// <param name="instance">实例</param>
    /// <param name="genaricTypes">泛型参数</param>
    /// <param name="args">参数</param>
    /// <returns>object||VoidResult</returns>
    public delegate object InvokerDelegate(object instance,Type[] genaricTypes,params object[] args);
    /// <summary>
    /// 成员调用器
    /// </summary>
    public interface IDynamicInvoker
    {
        /// <summary>
        /// 执行成员调用
        /// </summary>
        /// <param name="obj">实例</param>
        /// <param name="parameters">参数</param>
        /// <returns>object||VoidResult</returns>
        object Invoke(object obj, params object[] parameters);
        /// <summary>
        /// 初始化设置调用器
        /// </summary>
        void InitInvoker();
        /// <summary>
        /// 调用函数，用于复写
        /// </summary>
        Func<object, object[], object> OverwriteInvoker { get; set; }
        /// <summary>
        /// 成员信息
        /// </summary>
        MemberInfo MemberInfo { get; set; }

    }
}
