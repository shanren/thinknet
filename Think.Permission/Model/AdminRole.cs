﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 管理员角色
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminRole : EntityBase
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
