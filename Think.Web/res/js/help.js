﻿//设置搜索栏时间
function SetDate(TimeStartClientID, TimeEndClientID, dateFmt) {
    var TimeStart = F(TimeStartClientID);
    var TimeEnd = F(TimeEndClientID);

    TimeStart.onTriggerClick = function () {
        WdatePicker({
            el: TimeStartClientID + '-inputEl',
            dateFmt: dateFmt,
            maxDate: '#F{$dp.$D(\'' + TimeEndClientID + '-inputEl\')}',
            onpicked: function () {
                // 确认选择后，执行触发器输入框的客户端验证
                TimeStart.validate();
            }
        });
    }

    TimeEnd.onTriggerClick = function () {
        WdatePicker({
            el: TimeEndClientID + '-inputEl',
            dateFmt: dateFmt,
            minDate: '#F{$dp.$D(\'' + TimeStartClientID + '-inputEl\')}',
            onpicked: function () {
                // 确认选择后，执行触发器输入框的客户端验证
                TimeEnd.validate();
            }
        });
    }
}

//列表清除默认时间
function SetNullDate(grid, ce) {
    var cls = ce.split(',');
    var store = grid.getStore(), view = grid.getView(), storeCount = store.getCount();
    for (var i = 0; i < storeCount; i++) {
        var node = Ext.get(view.getNode(i));
        if (node == null) break;

        for (var j = 0; j < cls.length; j++) {
            var cl = cls[j];
            var cell = $(node).attr("cells")[cl];
            var text;
            text = cell.innerText || cell.textContent;
            text = $.trim(text);
            if (text == "1970-1-1 0:00:00" || text == "1970/1/1 0:00:00") {
                $(cell).text("");
            }
        }

    }

}