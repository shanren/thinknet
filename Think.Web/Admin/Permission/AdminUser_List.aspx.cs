﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Controls;
using Think.Data;
using Think.Permission.Model;
using Think.Web.App_Code;

namespace Think.Web.Admin.Permission
{
    public partial class AdminUser_List : AdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected override void BindGrid()
        {
            GridList.LoadData(string.Format("select * from AdminUser where {0}", SearcherColletion.GetWhere(this.Page, "basic")));
        }

        protected void btnOpenDialog_Click(object sender, EventArgs e)
        {
            OpenDialogClick(sender, "AdminUser_Edit.aspx", "管理员编辑", GridList, WindowOpen);
        }
    }
}