﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Think.Core.Interfaces
{
    /// <summary>
    /// Key
    /// </summary>
    public interface IKey
    {
        /// <summary>
        /// 取主键值
        /// </summary>
        /// <returns></returns>
        object GetKey();
    }

    /// <summary>
    /// 泛型key
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IKey<TKey> : IKey
    {
        /// <summary>
        /// 主键
        /// </summary>
        TKey DbKey { get; set; }
    }
}
