﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Think.Web.Admin.Login" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title>ThinkNet框架</title>
    <style>
        .login-image {
            background-color: #efefef;
            border-right: solid 1px #ddd;
        }
            .login-image img {
                width: 160px;
                height: 160px;
                padding: 10px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <f:pagemanager id="PageManager1" runat="server"></f:pagemanager>
        <f:window id="Window1" runat="server" ismodal="false" hidden="false" enableclose="false"
            enablemaximize="false" windowposition="GoldenSection" icon="Key" title="ThinkNet框架"
            layout="HBox" boxconfigalign="Stretch" boxconfigposition="Start" width="500px">
            <Items>
                <f:Image ID="imageLogin" ImageUrl="../res/images/login_3.png" runat="server"
                    CssClass="login-image">
                </f:Image>
                <f:SimpleForm ID="SimpleForm1" LabelAlign="Top" BoxFlex="1" runat="server"
                    BodyPadding="30px 20px" ShowBorder="false" ShowHeader="false">
                    <Items>
                        <f:TextBox ID="txtUserName" FocusOnPageLoad="true" runat="server" Label="帐号" Required="true"
                            ShowRedStar="true" Text="">
                        </f:TextBox>
                        <f:TextBox ID="txtPassword" TextMode="Password" runat="server" Required="true" ShowRedStar="true"
                            Label="密码" Text="">
                        </f:TextBox>
                    </Items>
                </f:SimpleForm>
            </Items>
            <Toolbars>
                <f:Toolbar ID="Toolbar1" runat="server" Position="Bottom" ToolbarAlign="Right">
                    <Items>
                        <f:Button ID="btnSubmit" Icon="LockOpen" Type="Submit" runat="server" ValidateForms="SimpleForm1" Text="登录" OnClick="btnSubmit_Click">
                        </f:Button>
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:window>
    </form>
</body>
</html>
