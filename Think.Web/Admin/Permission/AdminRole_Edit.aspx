﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminRole_Edit.aspx.cs" Inherits="Think.Web.Admin.Permission.AdminRole_Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager ID="PageManager1" runat="server"></f:PageManager>
        <f:Form ID="FormSubmit" ShowBorder="false" ShowHeader="false" runat="server" BodyPadding="10px" Title="SimpleForm">
            <Toolbars>
                <f:Toolbar ID="Toolbar1" runat="server">
                    <Items>
                        <f:Button ID="btnSave" Icon="DatabaseSave" runat="server" Text="保存"  OnClick="btnSave_Click" ValidateForms="FormSubmit"></f:Button>
                    </Items>
                </f:Toolbar>
            </Toolbars>
            <Rows>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtName" SqlField="Name" MaxLength="50" runat="server" Label="角色名称">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
                <f:FormRow>
                    <Items>
                        <t:ThinkTextBox ID="txtDescription" SqlField="Description" MaxLength="100" runat="server" Label="角色描述">
                        </t:ThinkTextBox>
                    </Items>
                </f:FormRow>
            </Rows>
        </f:Form>
    </form>
</body>
</html>
