﻿using System;
using System.Text;

namespace Think.Utils
{
    /// <summary>
    ///     随机数工具
    /// </summary>
    public abstract class Rand
    {
        private static readonly Random rand = new Random(DateTime.Now.Second);

        /// <summary>
        ///     返回非负随机数
        /// </summary>
        /// <returns></returns>
        public static int GetRandom()
        {
            return rand.Next();
        }

        /// <summary>
        ///     返回一个小于所指定最大值的非负随机数。
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static int GetRandom(int maxValue)
        {
            return rand.Next(maxValue);
        }

        /// <summary>
        ///     返回一个指定范围内的随机数。
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static int GetRandom(int minValue, int maxValue)
        {
            return rand.Next(minValue, maxValue);
        }

        /// <summary>
        ///     随机生成字符串
        /// </summary>
        /// <returns></returns>
        public static string CreateRandomString(int length)
        {
            var checkCode = String.Empty;
            for (var i = 0; i < length; i++)
            {
                //随机产生一个整数                             
                var number = rand.Next();
                //如果随机数是偶数 取余选择从[0-9]                             
                char code;
                if (number%2 == 0)
                    code = (char) ('0' + (char) (number%10));
                else
                    //如果随机数是奇数 选择从[A-Z]                                    
                    code = (char) ('A' + (char) (number%26));
                checkCode += code.ToString();
            }
            return checkCode;
        }

        #region 从字符串里随机得到，规定个数的字符串

        /// <summary>
        /// 从字符串里随机得到，规定个数的字符串.
        /// </summary>
        /// <param name="allChar"></param>
        /// <param name="CodeCount"></param>
        /// <returns></returns>
        private string GetRandomCode(string allChar, int CodeCount)
        {
            //string allChar = "1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,i,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z"; 
            string[] allCharArray = allChar.Split(',');
            string RandomCode = "";
            int temp = -1;
            Random rand = new Random();
            for (int i = 0; i < CodeCount; i++)
            {
                if (temp != -1)
                {
                    rand = new Random(temp * i * ((int)DateTime.Now.Ticks));
                }

                int t = rand.Next(allCharArray.Length - 1);

                while (temp == t)
                {
                    t = rand.Next(allCharArray.Length - 1);
                }

                temp = t;
                RandomCode += allCharArray[t];
            }
            return RandomCode;
        }

        #endregion

        #region 生成指定长度纯数字随机码
        /// <summary>
        /// 生成指定长度的纯数字或纯字母随机码
        /// </summary>
        /// <param name="FormatType">格式化类型，指定是数字还是字母，1为数字，2为字母,其他数字为字母数字混合</param>
        /// <param name="FormatLength">格式化的长度</param>
        /// <returns>格式化后的字符串</returns>
        public static string CreateRandom(string FormatType, int FormatLength)
        {
            if (FormatType == "1")
            {
                return GetRandom(FormatLength, "0123456789");
            }
            else if (FormatType == "2")
            {
                return GetRandom(FormatLength, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
            }
            return GetRandom(FormatLength, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        }

        public static string CreateRandom(int length, int type = 0)
        {
            return CreateRandom(type.ToString(), length);
        }

        private static string GetRandom(int formatLength, string chars)
        {
            StringBuilder num = new StringBuilder();
            Random rnd = new Random(new Guid().GetHashCode());
            for (int i = 0; i < formatLength; i++)
            {
                num.Append(chars[rnd.Next(0, chars.Length)].ToString());
            }
            return num.ToString();
        }

        #endregion
    }
}