﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using Think.Extend;
using Think.Utils;

namespace Think.Utils
{
    /// <summary>
    ///     下载文件
    /// </summary>
    public abstract class Net
    {

        /// <summary>
        /// 下载文件到客户端
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public static void DownFile(string filePath, string fileName)
        {
            var Response = HttpContext.Current.Response;

            //指定块大小   
            long chunkSize = 102400;
            //建立一个100K的缓冲区   
            var buffer = new byte[chunkSize];
            //已读的字节数   
            long dataToRead = 0;
            FileStream stream = null;
            try
            {
                //打开文件   
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                dataToRead = stream.Length;
                //添加Http头   
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachement;filename=" + fileName);
                Response.AddHeader("Content-Length", dataToRead.ToString());
                while (dataToRead > 0)
                {
                    if (Response.IsClientConnected)
                    {
                        var length = stream.Read(buffer, 0, Convert.ToInt32(chunkSize));
                        Response.OutputStream.Write(buffer, 0, length);
                        Response.Flush();
                        buffer = new Byte[10000];
                        dataToRead -= length;
                    }
                    else
                    {
                        //防止client失去连接  
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex) { Response.Write("Error:" + ex.Message); }
            finally
            { if (stream != null) { stream.Close(); } Response.Close(); }
        }

        /// <summary>
        /// 下载文件到客户端
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public static void DownStream(string s, string fileName, string contentType = "application/octet-stream")
        {
            var response = HttpContext.Current.Response;

            response.ContentEncoding = Encoding.UTF8;
            response.AppendHeader("content-disposition", "attachment;filename=\"" + HttpUtility.UrlEncode(fileName, Encoding.UTF8) + "\"");
            response.ContentType = contentType;

            response.Write(s);
            response.End();
        }

        public static string Post(string url, string postData)
        {
            var encoding = new ASCIIEncoding();
            var data = encoding.GetBytes(postData);

            var myRequest = (HttpWebRequest)WebRequest.Create(url);

            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = data.Length;
            using (var newStream = myRequest.GetRequestStream())
            {
                newStream.Write(data, 0, data.Length);
                newStream.Close();
            }

            using (var myResponse = (HttpWebResponse)myRequest.GetResponse())
            {
                var reader = new StreamReader(myResponse.GetResponseStream(), Encoding.Default);
                return reader.ReadToEnd();
            }
        }

    }
}