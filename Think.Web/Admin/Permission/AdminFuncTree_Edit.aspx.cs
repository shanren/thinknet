﻿using FineUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Data;
using Think.Permission;
using Think.Permission.Model;
using Think.Utils;
using Think.Utils.Web;
using Think.Web.App_Code;

namespace Think.Web.Admin.Permission
{
    public partial class AdminFuncTree_Edit : AdminPage
    {
        AdminFuncTree adminFunc;
        private static string[] CommonButton = { "btnNew", "btnUpdate", "btnDelete", "btnView", "btnFreeze", "btnOpen", "btnExcel", "btnImport" };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_ddlParent();
                Bind_Action();
                if ((adminFunc = GetModelByID<AdminFuncTree>()) != null)
                {
                    LoadData(this.Controls, adminFunc);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool isNew = false;
            if ((adminFunc = GetModelByID<AdminFuncTree>()) == null)
            {
                adminFunc = new AdminFuncTree();
                isNew = true;
            }
            LoadEntity(this.Controls, adminFunc);

            string funcID = ddlParent.SelectedValue;
            if (funcID == "0")
                adminFunc.TreeLevel = 0;//根导航
            else
                adminFunc.TreeLevel = 1;//页面菜单

            if (isNew)
            {
                ActionID = TRuntime.GetDB().Insert(adminFunc).ToString();
            }
            else
            {
                ActionID = adminFunc.ID.ToString();
                TRuntime.GetDB().Update(adminFunc);
            }

            var l = Get_Action(TypeTo.ToInt(ActionID));
            if (l != null && l.Count > 0)
            {
                Manage.SaveFuncTree(l, ActionID);
            }

            ShowAlert("操作完成", true);
        }

        //绑定父级菜单
        public void Bind_ddlParent()
        {
            ddlParent.DataTextField = "FuncName";
            ddlParent.DataValueField = "ID";
            ddlParent.DataSimulateTreeLevelField = "TreeLevel";
            ddlParent.DataSource = Manage.GetAllFuncTree();
            ddlParent.DataBind();
            ddlParent.Items.Insert(0, new FineUI.ListItem("根目录", "0"));
        }

        //绑定操作
        public void Bind_Action()
        {
            ActionID = RequestUtil.GetQueryString("ActionID");
            var list = Manage.GetActionTree(ActionID);
            if (list != null)
            {
                string[] selectedValues = new string[list.Count()];
                List<string> arr = new List<string>();
                int i = 0;
                foreach (AdminFuncTree v in list)
                {
                    if (CommonButton.Contains(v.Action))
                    {
                        selectedValues[i] = v.Action;
                    }
                    else
                    {
                        txtFunction.Text += v.FuncName + ":" + v.Action + ",";
                    }
                    i++;
                }
                cblFunction.SelectedValueArray = selectedValues;
            }
        }

        //设置操作
        public List<AdminFuncTree> Get_Action(int funcID)
        {
            CheckItem[] btnArray = cblFunction.SelectedItemArray;
            List<AdminFuncTree> listTree = new List<AdminFuncTree>();

            foreach (CheckItem btn in btnArray)
            {
                AdminFuncTree tree = new AdminFuncTree();
                tree.FuncName = btn.Text;
                tree.Action = btn.Value;
                tree.TreeLevel = 2;
                tree.FuncParentID = funcID;
                listTree.Add(tree);
            }
            if (!string.IsNullOrEmpty(txtFunction.Text.Trim()))
            {
                string[] actions = txtFunction.Text.Trim().Split(new char[2] { ';', '；' });
                if (actions != null && actions.Length > 0)
                {
                    foreach (string act in actions)
                    {
                        string[] temp = act.Split(new char[2] { ':', '：' });
                        if (temp.Length > 1 && !string.IsNullOrWhiteSpace(temp[0]) && !string.IsNullOrWhiteSpace(temp[1]))
                        {
                            AdminFuncTree tree = new AdminFuncTree();
                            tree.FuncName = temp[0];
                            tree.Action = temp[1];
                            tree.TreeLevel = 2;
                            tree.FuncParentID = funcID;
                            listTree.Add(tree);
                        }
                    }
                }
            }
            return listTree;
        }

    }
}