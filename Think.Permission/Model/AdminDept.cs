﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Think.Data;
using Think.Model;

namespace Think.Permission.Model
{
    /// <summary>
    /// 部门
    /// </summary>
    [PrimaryKey("ID")]
    public class AdminDept : EntityBase
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 父节点
        /// </summary>
        public int ParentID { get; set; }
    }
}
