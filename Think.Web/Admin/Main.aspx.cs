﻿using FineUI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Think.Permission;
using Think.Permission.Model;
using Think.Web.App_Code;

namespace Think.Web.Admin
{
    public partial class Main : AdminPage
    {
        /// <summary>
        /// 左侧菜单树
        /// </summary>
        public Tree treeMenu;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<AdminFuncTree> menus = Manage.GetAdminTree(AdminLoginUser.ID);
                if (menus == null || menus.Count == 0)
                {
                    Alert.Show("系统管理员尚未给你配置菜单");
                    return;
                }
                else
                {
                    treeMenu = InitTreeMenu(menus);
                    txtUser.Text = "欢迎您，" + AdminLoginUser.LoginName;

                    // 注册采购商端脚本，服务器端控件ID和采购商端ID的映射关系
                    JObject ids = GetClientIDS(mainTabStrip, regionLeft);
                    ids.Add("mainMenu", treeMenu.ClientID);
                    ids.Add("menuType", "menu");
                    string idsScriptStr = String.Format("window.IDS={0};", ids.ToString(Newtonsoft.Json.Formatting.None));
                    PageContext.RegisterStartupScript(idsScriptStr);
                }
            }
        }


        protected void btnExit_Click(object sender, EventArgs e)
        {
            if (Manage.ExitLogin(AdminLoginUser.LoginName))
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                Alert.Show("请重新退出");
            }
        }


        #region 生成树

        /// <summary>
        /// 创建树菜单
        /// </summary>
        /// <param name="menus"></param>
        /// <returns></returns>
        private Tree InitTreeMenu(List<AdminFuncTree> menus)
        {
            Tree treeMenu = new Tree();
            treeMenu.ID = "treeMenu";
            treeMenu.EnableArrows = true;
            treeMenu.ShowBorder = false;
            treeMenu.ShowHeader = false;
            treeMenu.EnableIcons = true;
            treeMenu.AutoScroll = true;
            treeMenu.EnableSingleClickExpand = true;
            regionLeft.Items.Add(treeMenu);

            // 生成树
            ResolveMenuTree(menus, 0, treeMenu.Nodes);

            // 展开第一个树节点
            treeMenu.Nodes[0].Expanded = true;

            return treeMenu;
        }

        /// <summary>
        /// 生成菜单树
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="parentMenuId"></param>
        /// <param name="nodes"></param>
        private void ResolveMenuTree(List<AdminFuncTree> menus, int parentMenuId, FineUI.TreeNodeCollection nodes)
        {
            foreach (AdminFuncTree menu in menus)
            {
                if (menu.FuncParentID == parentMenuId)
                {
                    FineUI.TreeNode node = new FineUI.TreeNode();
                    nodes.Add(node);

                    node.Text = menu.FuncName;
                    if (!string.IsNullOrWhiteSpace(menu.FuncUrl))
                    {
                        node.NodeID = menu.FuncUrl.Replace(".aspx", "").Replace("/", "_");
                    }
                    if (!string.IsNullOrWhiteSpace(menu.FuncUrl))
                    {
                        node.NavigateUrl = ResolveUrl(menu.FuncUrl);
                    }
                    ResolveMenuTree(menus, menu.ID, node.Nodes);
                }
            }
        }

        private JObject GetClientIDS(params ControlBase[] ctrls)
        {
            JObject jo = new JObject();
            foreach (ControlBase ctrl in ctrls)
            {
                jo.Add(ctrl.ID, ctrl.ClientID);
            }
            return jo;
        }


        #endregion
    }
}