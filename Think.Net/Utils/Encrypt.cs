using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using Think.Extend;

namespace Think.Utils
{
    /// <summary>
    ///     加密工具
    /// </summary>
    public abstract class Encrypt
    {
        /// <summary>
        ///     MD5函数
        /// </summary>
        /// <param name="str">原始字符串</param>
        /// <param name="isReverse">是否加密后反转字符串</param>
        /// <param name="ToUpper">是否加密后转为大写</param>
        /// <param name="count">加密次数</param>
        /// <returns>MD5结果</returns>
        public static string MD5(string str, bool ToUpper = false, bool isReverse = false, int count = 1)
        {
            if (count <= 0) { return str; }

            var b = Encoding.Default.GetBytes(str);
            b = new MD5CryptoServiceProvider().ComputeHash(b);
            var md5 = string.Empty;
            for (var i = 0; i < b.Length; i++)
            {
                md5 += b[i].ToString("x").PadLeft(2, '0');
            }

            if (isReverse) { md5 = Reverse(md5); }
            if (ToUpper) { md5 = md5.ToUpper(); }

            return MD5(ToUpper ? md5.ToUpper() : md5.ToLower(), ToUpper, isReverse, --count);
        }

        /// <summary>
        ///     SHA256函数
        /// </summary>
        /// ///
        /// <param name="str">原始字符串</param>
        /// <returns>SHA256结果</returns>
        public static string SHA256(string str)
        {
            var SHA256Data = Encoding.UTF8.GetBytes(str);
            var Sha256 = new SHA256Managed();
            var Result = Sha256.ComputeHash(SHA256Data);
            return Convert.ToBase64String(Result); //返回长度为44字节的字符串
        }

        /// <summary>
        ///     反转字符串
        /// </summary>
        /// <param name="input">要反转字符串</param>
        /// <returns></returns>
        public static string Reverse(string input)
        {
            var chars = input.ToUpper().ToCharArray();
            var length = chars.Length;
            for (var index = 0; index < length / 2; index++)
            {
                var c = chars[index];
                chars[index] = chars[length - 1 - index];
                chars[length - 1 - index] = c;
            }
            return new String(chars);
        }

        #region Base64加密

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="code">明文</param>
        /// <returns>密文</returns>
        public static string EncodeBase64(string code)
        {
            string encode = "";
            byte[] bytes = Encoding.GetEncoding(Encoding.UTF8.CodePage).GetBytes(code);
            encode = Convert.ToBase64String(bytes);
            return encode;
        }

        #endregion

        #region Base64解密
        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="code">密文</param>
        /// <returns>明文</returns>
        public static string DecodeBase64(string code)
        {
            string decode = "";
            try
            {
                byte[] bytes = Convert.FromBase64String(code);
                decode = Encoding.GetEncoding(Encoding.UTF8.CodePage).GetString(bytes);
            }
            catch
            {
                decode = code;
            }
            return decode;
        }

        #endregion



        /// <summary>
        /// 压缩字符串
        /// </summary>
        public class ZipWrapper
        {
            /// <summary>
            /// 压缩
            /// </summary>
            /// <param name="str">字符串</param>
            public static string Compress(string str)
            {
                //因输入的字符串不是Base64所以转换为Base64,因为HTTP如果不传递Base64则会发生http 400错误
                return Convert.ToBase64String(Compress(Convert.FromBase64String(Convert.ToBase64String(Encoding.Default.GetBytes(str)))));
            }

            /// <summary>
            /// 解压
            /// </summary>
            /// <param name="str">字符串</param>
            public static string Decompress(string str)
            {
                return Encoding.Default.GetString(Decompress(Convert.FromBase64String(str)));
            }

            /// <summary>
            /// 压缩
            /// </summary>
            /// <param name="bytes">字节组</param>
            public static byte[] Compress(byte[] bytes)
            {
                using (var ms = new MemoryStream())
                {
                    var Compress = new GZipStream(ms, CompressionMode.Compress);
                    Compress.Write(bytes, 0, bytes.Length);
                    Compress.Close();
                    return ms.ToArray();
                }
            }

            /// <summary>
            /// 压缩
            /// </summary>
            /// <param name="bytes">字节组</param>
            public static byte[] Decompress(Byte[] bytes)
            {
                using (var tempMs = new MemoryStream())
                {
                    using (var ms = new MemoryStream(bytes))
                    {
                        var Decompress = new GZipStream(ms, CompressionMode.Decompress);
                        Decompress.CopyTo(tempMs);
                        Decompress.Close();
                        return tempMs.ToArray();
                    }
                }
            }
        }
    }
}